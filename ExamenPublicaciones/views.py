from django.shortcuts import render, redirect
from django.contrib.auth import login, logout
from Publicaciones.forms import *

def index(request):
    return render(request,'index.html')

def iniciar(request):
    if request.method == 'POST':
        form = CustomAuthForm(data=request.POST)
        if form.is_valid():
            usuario = form.get_user()
            login(request,usuario)
            return redirect('Publicaciones:index')
    else:
        form = CustomAuthForm()
    return render(request,'login.html',{'form':form})

def registro(request):
    registered = False
    if request.method == 'POST':
        form = UsuarioForm(data=request.POST)
        if form.is_valid():
            form = form.save()
            form.set_password(form.password)
            form.save()
            registered = True
            return redirect('Publicaciones:index')
    else:
        form = UsuarioForm()
    return render(request,'registro.html',{'form':form})

def salir(request):
	if request.method=='POST':
		logout(request)
		return redirect('index')
