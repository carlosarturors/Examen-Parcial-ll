from django import forms
from .models import *
import datetime
from django.utils import timezone
from django.forms import ModelForm
from django.contrib.auth.forms import AuthenticationForm
from django.forms.widgets import PasswordInput, TextInput

class UsuarioForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = ('username','password','nombre','paterno','materno',)
        labels = {
            'username': ('Introduzca su nombre de usuario'),
            'password': ('Introduzca su contraseña'),
            'nombre': ('Introduzca su nombre(s)'),
            'paterno': ('Introduzca su apellido paterno'),
            'materno': ('Introduzca su apellido materno'),
        }
        widgets = {
            'username': forms.TextInput(attrs={'id':'usuario', 'class':'validate'}),
            'password': forms.PasswordInput(attrs={'id':'password', 'class':'validate'}),
            'nombre': forms.TextInput(attrs={'id':'nombre', 'class':'validate'}),
            'paterno': forms.TextInput(attrs={'id':'paterno', 'class':'validate'}),
            'materno': forms.TextInput(attrs={'id':'materno', 'class':'validate'}),
        }

class PublicacionForm(forms.ModelForm):
    class Meta:
        model = Publicacione
        fields = ('titulo','cuerpo','categoria','slug',)
        labels = {
            'titulo': ('Introduzca el titulo'),
            'cuerpo': ('Introduzca el articulo'),
            'categoria': ('Seleccione una categoría'),
            'slug': ('Introduzca la dirección del articulo'),
        }
        widgets = {
            'titulo': forms.TextInput(attrs={'id':'titulo', 'class':'validate'}),
            'cuerpo': forms.TextInput(attrs={'id':'cuerpo', 'class':'validate'}),
            'slug': forms.TextInput(attrs={'id':'slug', 'class':'validate'}),
        }

class CustomAuthForm(AuthenticationForm):
    username = forms.CharField(widget=TextInput(attrs={'id':'usuario', 'class':'validate'})),
    password = forms.CharField(widget=PasswordInput(attrs={'id':'password', 'class': 'validate'})),