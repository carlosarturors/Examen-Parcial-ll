from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Usuario(User):
    nombre = models.CharField(max_length=40,null=False)
    paterno = models.CharField(max_length=40,null=False)
    materno = models.CharField(max_length=40,null=False)

    def __str__(self):
        return self.nombre + " " + self.paterno + " " + self.materno 

class Categoria(models.Model):
    categoria = models.CharField(max_length=50)

    def __str__(self):
        return self.categoria

class Publicacione(models.Model):
    titulo = models.CharField(max_length=50)
    cuerpo = models.TextField(max_length=900)
    slug = models.CharField(max_length=50,null=False)
    categoria = models.ForeignKey(Categoria,on_delete=models.CASCADE)
    autor = models.ForeignKey(Usuario, on_delete=models.CASCADE)

    def __str__(self):
        return self.titulo