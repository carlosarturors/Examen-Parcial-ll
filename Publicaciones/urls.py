from django.contrib import admin
from django.urls import path, re_path, include
from . import views

app_name = "Publicaciones" 

urlpatterns = [
    path('', views.index, name='index'),
    re_path('(?P<slug>[\w-]+)/$', views.publicacion, name='publicacion'),
]